
# Logo and Favicon Web Crawler

Crawler is logo and favicon crawler written in Python.
Its objective is to collect as many logos as you can across across a sample of websites.


## Features

- Import a csv file containing website urls and watch it magically grab and return the logo and favicon links for each website.


## Tech

Crawler uses a number of open source projects to work properly:

- [Aiohttp] - Asynchronous HTTP Client/Server for asyncio and Python.
- [BeautifulSoup] - Python library for pulling data out of HTML and XML files.


## Installation and Usage
Crawler uses nixos for package management.
There are two versions of crawler included in this repository:
1. Synchronous version
2. Asynchronous version
 
Run the following command at the root of the project to execute the synchronous version of crawler:
```
nix-shell py/logocrawler/crawler.py <path_to_csv_file>
```

Run the following command at the root of the project to execute the asynchronous version of crawler:
```
nix-shell py/logocrawler/async_crawler.py <path_to_csv_file>
```



## License

MIT

**Free Software, Hell Yeah!**

