with import <nixpkgs> {};
let
  pythonEnv = python38.withPackages (ps: [
  ]);
in mkShell {
  packages = [
    pkgs.python3Packages.beautifulsoup4
    pkgs.python3Packages.aiohttp
  ];
}