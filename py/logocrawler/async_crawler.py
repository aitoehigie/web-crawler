#!/usr/bin/python3
import argparse
import sys
import csv
import re
import asyncio
from timeit import default_timer

from bs4 import BeautifulSoup
import aiohttp

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0"
}


def scraper(url, page):
    soup = BeautifulSoup(page, "html.parser")
    favicon = soup.find(
        "link", attrs={"rel": re.compile("^(shortcut icon|icon)$", re.I)}
    )
    logo = (
        soup.find(["img", "svg"], attrs={"id": re.compile(r"logo", re.I)})
        or soup.find(["img", "svg"], attrs={"class": re.compile(r"logo", re.I)})
        or soup.find(["img", "svg"], attrs={"alt": re.compile(r"logo", re.I)})
        or soup.find(["img", "svg"], attrs={"src": re.compile(r"logo", re.I)})
    )
    favicon_url = favicon.get("href") if favicon else f"{url}/favicon.ico"
    logo_url = logo.get("href", logo.get("src")) if logo else "Not Found"
    return (url, logo_url, favicon_url)


async def fetch(session, url):
    try:
        async with session.get(url) as r:
            if r.status != 200:
                r.raise_for_status()
            return await r.text()
    except Exception as e:
        print(str(e))


async def fetch_all(session, urls):
    tasks = []
    for url in urls:
        task = asyncio.create_task(fetch(session, url))
        tasks.append(task)
    resp = await asyncio.gather(*tasks)
    return resp


async def main(args):
    t1 = default_timer()
    urls = []
    try:
        with open(args.input, "r") as file:
            for url in file.readlines():
                urls.append(url.strip().rstrip(","))
        urls = ["https://www." + url for url in urls if not url.startswith("https://")]
        fields = ["url", "logo", "favicon"]
        writer = csv.writer(sys.stdout)
        writer.writerow(fields)
        async with aiohttp.ClientSession(headers=headers) as session:
            pages = await fetch_all(session, urls)
        for url, page in zip(urls, pages):
            writer.writerow(scraper(url, page))
    except IOError:
        msg = "Error: can't find the file or read data."
        print(msg)
    except FileNotFoundError:
        msg = f"Sorry, the file {args.input} does not exist."
        print(msg)
    print(f"Execution time: {default_timer() - t1}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="crawler",
        usage="%(prog)s input output",
        description="Crawl a list of websites and return their logo and favicon urls.",
        epilog="Enjoy the program! \N{grinning face}",
    )
    parser.add_argument(
        "input", help="Input file containing a list of websites to be crawled."
    )
    args = parser.parse_args()
    asyncio.run(main(args))
