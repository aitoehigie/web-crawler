#!/usr/bin/python3
import sys
import csv
import argparse
import re
from timeit import default_timer
import urllib3
from bs4 import BeautifulSoup

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0"
}


def scraper(url):
    http = urllib3.PoolManager()
    page = http.request("GET", url, headers=headers)
    soup = BeautifulSoup(page.data, "html.parser")
    favicon = soup.find(
        "link", attrs={"rel": re.compile("^(shortcut icon|icon)$", re.I)}
    )
    logo = (
        soup.find(["img", "svg"], attrs={"id": re.compile(r"logo", re.I)})
        or soup.find(["img", "svg"], attrs={"class": re.compile(r"logo", re.I)})
        or soup.find(["img", "svg"], attrs={"alt": re.compile(r"logo", re.I)})
        or soup.find(["img", "svg"], attrs={"src": re.compile(r"logo", re.I)})
    )
    favicon_url = favicon.get("href") if favicon else f"{url}/favicon.ico"
    logo_url = logo.get("href", logo.get("src")) if logo else "Not Found"
    return (url, logo_url, favicon_url)


def main(args):
    t1 = default_timer()
    try:
        with open(args.input, "r") as file:
            fields = ["url", "logo", "favicon"]
            writer = csv.writer(sys.stdout)
            writer.writerow(fields)
            for url in file.readlines():
                writer.writerow(scraper(url.strip().rstrip(",")))
    except IOError:
        msg = "Error: can't find the file or read data."
        print(msg)
    except FileNotFoundError:
        msg = f"Sorry, the file {args.input} does not exist."
        print(msg)
    print(f"Execution time: {default_timer() - t1}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="crawler",
        usage="%(prog)s input output",
        description="Crawl a list of websites and return their logo and favicon urls.",
        epilog="Enjoy the program! \N{grinning face}",
    )
    parser.add_argument(
        "input", help="Input file containing a list of websites to be crawled."
    )
    args = parser.parse_args()
    main(args)
